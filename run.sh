#!/usr/bin/env bash

if [[ -z "${DISTRO}" ]]; then
  echo 'Please: `export DISTRO="stretch"`'
  exit 1
fi

if [[ -z "${SERVERNAME}" ]]; then
  echo 'Please: `export SERVERNAME="example.org.uk"`'
  exit 1
fi

echo "Running: ansible-playbook nextcloud.yml -i ${SERVERNAME}, -e \"hostname=${SERVERNAME}\" -v"
ansible-playbook nextcloud.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
echo "Running: ansible-playbook collabora.yml -i ${SERVERNAME}, -e \"hostname=${SERVERNAME}\" -v" 
ansible-playbook collabora.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
echo "Running: ansible-playbook coturn.yml -i ${SERVERNAME}, -e \"hostname=${SERVERNAME}\" -v"
ansible-playbook coturn.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
echo "Running: ansible-playbook prosody.yml -i ${SERVERNAME}, -e \"hostname=${SERVERNAME}\" -v" 
ansible-playbook prosody.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v


