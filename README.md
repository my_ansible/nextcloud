## Install

Ansible Playbook to install [Nextcloud](https://nextcloud.com/), first install python and run a Playbook to create suders and add your public `ssh` keys (if the server is being set up to run on a Webarchitects VPS then please also run the [Ansible Playbook](https://git.coop/webarch/ansible) and an appropriate sudoers one) and then:

```bash
export SERVERNAME="nextcloud.example.org"
ansible-playbook nextcloud.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
```

A `admin` user account will be created and the random password for this user can be found in `/root/.nextcloud`.

### Collabora

If you want [Collabora](https://nextcloud.com/collaboraonline/) then make sure to set a hostname up for Collabora, eg `collabora.nextcloud.example.org` and point it at the same IP address as you set for `nextcloud.example.org` and then run:

```bash
export SERVERNAME="nextcloud.example.org"
ansible-playbook collabora.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
```

### Coturn

A TURN (Traversal Using Relay NAT) server is needed for Nextcloud Talk when both people of an different networks and using NAT, the following is based on [this write up](https://blog.netways.de/2017/08/16/setting-up-a-turn-server-for-nextcloud-video-calls/) of how to setup Coturn.

```bash
export SERVERNAME="nextcloud.example.org"
ansible-playbook coturn.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
```

### Prosody

If you want to use [JavaScript XMPP Chat](https://apps.nextcloud.com/apps/ojsxc) then you first need to [setup the DNS entries](https://github.com/jsxc/xmpp-cloud-auth/wiki/dns) and then install Prosody:

```bash
export SERVERNAME="nextcloud.example.org"
ansible-playbook prosody.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
```

## DNS

An example DNS configuration for a server where the Ansible `{{ hostname }}` is  `nextcloud.example.org.uk` at `1.2.3.4` that has the option to host private chat rooms for people with accounts on the server (`conference.nextcloud.example.org.uk`)  and public ones (`public.nextcloud.example.org.uk`) open to all:

```
nextcloud                                 IN      A       1.2.3.4
www.nextcloud                             IN      A       1.2.3.4
collabora.nextcloud                       IN      A       1.2.3.4
; only local nextcloud account holders can join conference chat rooms
conference.nextcloud                      IN      A       1.2.3.4 
; public chat rooms can be joined by people on other servers
public.nextcloud                          IN      A       1.2.3.4
_xmppconnect.nextcloud                    IN      TXT     "_xmpp-client-xbosh=https://nextcloud.example.org.uk/http-bind/"
_xmpp-client._tcp.nextcloud               SRV     0       5 5222 nextcloud.example.org.uk.
_xmpp-server._tcp.nextcloud               SRV     0       5 5269 nextcloud.example.org.uk.
_xmpp-server._tcp.public.nextcloud        SRV     0       5 5269 public.nextcloud.example.org.uk.
```
See the [Prosody DNS configuration in Jabber/XMPP page](https://prosody.im/doc/dns).

## Upgrade

See the [documentation](https://docs.nextcloud.com/server/12/admin_manual/maintenance/update.html) for the different Nextcloud upgrade methods, using the command line (this is the best way to do it, the `nextcloud_upgrade.yml` Playbook is a work in progress):

```bash
sudo -i
su - www-data -s /bin/bash
cd nextcloud/updater/
php updater.phar
exit
exit
```

To upgrade Collabora Online either re-run the Collabora Playbook:

```bash
export SERVERNAME="nextcloud.example.org"
ansible-playbook collabora.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}" -v
```

Or do it manually:


```bash
sudo -i
export CONTAINER=$(docker ps -q)
docker stop ${CONTAINER}
docker pull collabora/code
export ESCAPED_HOSTNAME=$(escape-domain.sh $(hostname -f))
docker run -t -d -p 127.0.0.1:9980:9980 -e "domain=${ESCAPED_HOSTNAME}" --restart always --cap-add MKNOD collabora/code
```

The old Collabora Online Docker Edition containers take up a lot of space, after an update check that the latest version is runnng and working OK via the web interface and then remove the ones that are not running:

```bash
sudo -i
docker image prune
```

Note that after a Collabora Online update you will probably need to do a `service docker restart` due to the issue below. 

## Troubleshooting

If you get the following error when trying to edit documents online:

> Failed to load the document. Please ensure the file type is supported and not corrupted, and try again.

Then try restarting Docker:

```bash
service docker restart
``` 

See [this thread](https://help.nextcloud.com/t/failure-please-ensure-the-file-type-is-supported-and-not-corrupted/8031).

### Prosody / xmpp-cloud-auth

Tests you can try:

```bash
cd /opt/xmpp-cloud-auth
./xcauth.py --isuser-test admin $( hostname -f )
./xcauth.py --roster-test admin $( hostname -f )
```


## TODO

* [Spreed.me and a TURN server](https://help.nextcloud.com/t/complete-nc-installation-on-debian-with-spreed-me-and-turn-step-by-step/2436)
* [Video calls in Nextcloud 11 with Spreed](https://nextcloud.com/blog/video-calls-in-nextcloud-11-with-spreed/)
* [Setting up a TURN Server for Nextcloud Video Calls](https://blog.netways.de/2017/08/16/setting-up-a-turn-server-for-nextcloud-video-calls/)
* [Docker Coturn](https://hub.docker.com/r/steppechange/docker-coturn/) see the [GitHub project](https://github.com/SteppeChange/docker-coturn)
* [Docker Spreed](https://hub.docker.com/r/spreed/webrtc/) see the [GitHub project](https://github.com/strukturag/spreed-webrtc)

