#!/usr/bin/env bash

# This script restarts Prosody when xmpp-cloud-auth appears to prevent 
# `service prosody restart` from working 

# check that the script is being run by root
if [[ "$(id -u)" != "0" ]] ; then
  echo "You must run '$0' as root or via sudo" 
  exit 1
fi

# Stop Prosody
echo "Stopping Prosody"
service prosody stop

# Check what is listening on port 5222
NETSTAT=$(netstat -tulpn | grep '0.0.0.0:5222' | awk '{ print $7}' | awk -F '/' '{ print $2}')
if [[ "${NETSTAT}" == "python" ]]; then
  echo "Killing xcauth.py as python is listening on port 5222"
  killall -9 xcauth.py
elif [[ "${NETSTAT}" == "lua5.1" ]]; then
  echo "Killing lua5.1 as it is listening on port 5222"
  killall -9 lua5.1
elif [[ -z "${NETSTAT}" ]]; then
  echo "Prosody has been stopped without the need to kill processes"
else
  echo "Process ${NETSTAT} is listening on port 5222 and this needs to be manually killed"
  exit 1
fi

# Start Prosody
echo "Starting Prorody"
service prosody start
exit 0
 



