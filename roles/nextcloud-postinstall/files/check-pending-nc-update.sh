#!/bin/bash

if [ ! -p /dev/stdin ]; then
        #nothing got piped into this script
        echo ""
        exit
fi

pending_version=$(cat)

if [[ ! -n "${pending_version/[ ]*\n/}" ]]; then
        #pending_version is empty or contains only spaces
        echo ""
        exit
fi

installed_version=$(xmllint --xpath '/ocs/data/nextcloud/system/version/text()' /var/tmp/nc-stats.xml)

if [ $installed_version == $pending_version ]; then
          echo ""
          exit
fi

echo $pending_version
