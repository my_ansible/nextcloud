#!/usr/bin/python2.7

# This script is run by www-data

import sys, yaml
import subprocess

apps = yaml.load(sys.stdin)

result = 0
for app in apps['Enabled']:
    app_name = app.keys()[0]
    app_version = app[app_name]
    #cmd = 'sudo -u www-data /usr/bin/php /var/www/nextcloud/occ config:app:get updatenotification %s' % app_name
    cmd = '/usr/bin/php /var/www/nextcloud/occ config:app:get updatenotification %s' % app_name
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    pending_version = proc.stdout.read().rstrip()
    if pending_version and pending_version != app_version:
        #print app_name+' - '+app_version+' - '+pending_version
        result = result + 1

print result
